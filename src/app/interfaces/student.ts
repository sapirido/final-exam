export interface Student {
    studentId?:string;
    mathAvg:number;
    psychoGrade:number;
    payment:boolean;
    predict:string;
    ownedEmail:string;
}
