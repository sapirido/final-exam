import { AuthService } from './../auth.service';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { StudentService } from '../student.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {
  mathAvg:number;
  psychoGrade:number;
  paid:boolean = false;
  mathError:string = '';
  psychError:string = '';
  predictResult:string;
  selectedValue:boolean = false;
  result$:Observable<string>;
  ownedEmail:string;
  predictsOptions = [{value:false,viewValue:'Did not drop out of school'},{value:true,viewValue:'drop out of school'}];
  constructor(private studentService:StudentService,private auth:AuthService,private router:Router) { }

  onSubmit(){
    if(this.isValidForm()){
     this.predict();
    }
  }

  isValidForm(){
    if(this.mathAvg < 0 || this.mathAvg > 100){
      this.mathError ='**the grade must to be between 0 to 100**'
      return false;
    }
    if(this.psychoGrade > 800 || this.psychoGrade < 0){
      this.psychError ='**the grade must to be between 0 to 800**'
      return false;
    }
    return true;
  }

  predict(){
    this.result$ = this.studentService.predictStudent(this.mathAvg,this.psychoGrade,this.paid);
    this.result$.subscribe(
      res => {
        this.predictResult = res;
        if(Number(res) > 0.5){
          this.selectedValue = false;
        }else{
          this.selectedValue = true;
        }
      }
    )
  }

  saveStudent(){
     this.studentService.saveStudent(this.mathAvg,this.psychoGrade,this.paid,this.predictResult,this.ownedEmail).subscribe(
       res => this.router.navigate(['/students/list'])
     )
  }
  
  ngOnInit(): void {
    this.auth.getUser().subscribe(
      user => this.ownedEmail = user.email
    )
  }


  clearError(){
    this.psychError = '';
    this.mathError = '';
  }

}
