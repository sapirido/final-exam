import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Student } from '../interfaces/student';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {

  students$:Observable<any>;
  students:Student[] = []
 displayedColumns: string[] = ['mathAvg','psychoGrade','payment','predict','ownedEmail','delete'];
  
  constructor(private studentService:StudentService) { }

  deleteStudent(studentId:string){
    this.studentService.deleteStudent(studentId);
  }
  ngOnInit(): void {
    this.students$ = this.studentService.getStudents();
    this.students$.subscribe(
      docs => {
        this.students =[];
        for(let document of docs){
          const newStudent:Student = document.payload.doc.data();
          newStudent.studentId = document.payload.doc.id;
          this.students.push(newStudent);
        }
      }
    )
  }

}
