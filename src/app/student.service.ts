import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  studentCollection:AngularFirestoreCollection = this.db.collection('students');
  URL:string = "https://wckfilkq2g.execute-api.us-east-1.amazonaws.com/beta";
  constructor(private http:HttpClient,private db:AngularFirestore) { }

  predictStudent(math,psycho,payment){
    const body = {
      data:{
        math:math,
        psycho:psycho,
        payment:payment
      }
    }
    return this.http.post<any>(this.URL,body).pipe(
      map(res => {
        const final:string = res.body;
        console.log({final});
        return final;
      })
    )
  }
  saveStudent(mathAvg:number,psychoGrade:number,payment:boolean,predict:string,ownedEmail:string){
    const student = {
      mathAvg:mathAvg,
      psychoGrade:psychoGrade,
      payment:payment,
      predict:predict,
      ownedEmail:ownedEmail,
      addedTime:+new Date()
    }
    this.studentCollection.add(student);
    return this.studentCollection.snapshotChanges();
  }

  deleteStudent(studentId){
    this.studentCollection.doc(studentId).delete();
    return this.studentCollection.snapshotChanges();
  }

  getStudents(){
   
    this.studentCollection = this.db.collection('students',ref => ref.orderBy('addedTime','desc'));
    return this.studentCollection.snapshotChanges();
  }
}
